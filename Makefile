
export CXX = g++
MKDEP = $(CXX) -MM -o .depend
export CXXFLAGS = -std=c++11 -Wall -g
export LDFLAGS =-L/usr/lib/x86_64-linux-gnu
export LDLIBS =-lboost_system -lboost_filesystem -lboost_date_time
export INCLUDE = -I/home/cyliane/Downloads/boost_1_58_0/libs
SRC = $(wildcard *.cpp)
OBJ = $(SRC:.cpp=.o)
HDRS = $(wildcard *.hpp)

FILES = files files/*

EXEC = contact
DIR = DAL SRV Transverse
DIR_O = DAL/DALFileManager.o DAL/DALCrypte.o SRV/SRVContact.o Transverse/contact.o Transverse/global.o Transverse/usefulFunc.o Transverse/fileContact.o

all:$(EXEC)

$(EXEC): $(OBJ)
	@$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ $^ $(DIR_O) $(LDFLAGS) $(LDLIBS)

main.o: $(DIR_O)

%.o: %.cpp
	@$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<

.PHONY: depend clean cleanAll
depend: .depend
.depend: $(SRC) $(HDRS)
	@$(MKDEP) $(CXXFLAGS) $(SRC)

-include .depend

clean:
	@make -C SRV clean
	@make -C DAL clean
	@make -C Transverse clean
	@rm -rf *.o

cleanAll: clean
	@rm -rf $(EXEC)
	@rm -rf a.out

cleanFiles: cleanAll
	@rm -rf $(FILES)

go: cleanAll all