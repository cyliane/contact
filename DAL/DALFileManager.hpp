// DALFileManager

#ifndef DALFileManager_HPP
#define DALFileManager_HPP

#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <stdio.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>

#include "../Transverse/contact.hpp"



using namespace std;

class DALFileManager
{
	public:
		static DALFileManager& getInstance();
		static void kill();
		~DALFileManager() { }

		// stock to lines the data of the file
		void stockContentsFromFile(string path, stringstream* lines);

		// return a vector of the files present inside the contact folder
		vector<string> getVectContactList();

		// adding a new file
		void addingModifyNewFile(Contact *contact, int caller);

		// erase a file from a contact object
		void eraseContactFile(Contact* cont);

		// check if the file exist
		bool checkExistingFile(std::string fileName);

	private:
		DALFileManager();

		static DALFileManager* _singleton;

		std::string _currentFile;

		void writeAllTheTypeOfTheContactFile(Contact* contact, std::string filePath);
};

#endif /* DALFileManager_HPP */