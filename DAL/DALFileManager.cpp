// DALFileManager.cpp

#include <string>
#include <sstream>
#include <iostream>
#include <string.h>

#include "DALCrypte.hpp"
#include "DALFileManager.hpp"
#include "../Transverse/global.hpp"
#include "../Transverse/fileContact.hpp"

DALFileManager *DALFileManager::_singleton = nullptr;

DALFileManager& DALFileManager::getInstance()
{
	if (nullptr == _singleton)
	{
		_singleton = new DALFileManager();
	}

	return *_singleton;
}

void DALFileManager::kill()
{
	if (nullptr != _singleton)
	{
		delete _singleton;
		_singleton = nullptr;
	}
}

DALFileManager::DALFileManager()
{

}

// return a list of path for all the files present inside the folder contact
std::vector<std::string> DALFileManager::getVectContactList()
{
	typedef std::vector<boost::filesystem::path> vec;
	vec v;
	std::vector<std::string> svec;
	std::string tmp;

	// test if the folder exist and is a directory: g_dirContactFile_path
	boost::filesystem::path p(Global::getInstance().getFolderContactPath());
	if ((boost::filesystem::exists(p) == true) && (boost::filesystem::is_directory(p) == true))
	{
		copy(boost::filesystem::directory_iterator(p), boost::filesystem::directory_iterator(),
			back_inserter(v));

		sort(v.begin(), v.end());

		for (vec::const_iterator it(v.begin()); it != v.end(); ++it)
		{
			boost::filesystem::path ptmp = *it;
			tmp.assign(boost::filesystem::basename(ptmp));
			svec.push_back(boost::filesystem::basename(ptmp));
		}
	}
	else
	{
		std::cerr << "DALFileManager - getFileFromContactFolder - error folder doesn't exist !" << std::endl;
	}

	return svec;
}

//create the different type of information in the new contact file
void DALFileManager::writeAllTheTypeOfTheContactFile(Contact* contact, std::string filePath)
{
	// open the file
	std::ofstream file(filePath);

	if (!file.is_open())
	{
		std::cerr << "DALFileManager - writeAllTheTypeOfTheContactFile - openFile Failed." << std::endl;
		return;
	}

	// write inside the file
	file << "{" << std::endl;
	file << "\"lastname\" : \"" << contact->getLastname() << "\"," << std::endl;
	file << "\"firstname\" : \"" << contact->getFirstname() << "\"," << std::endl;
	file << "\"nickname\" : \"" << contact->getNickname() << "\"," << std::endl;
	file << "\"nbCellPhone\" : \"" << contact->getNbCellPhone() << "\"," << std::endl;
	file << "\"nbHome\" : \"" << contact->getNbHome() << "\"," << std::endl;
	file << "\"nbOffice\" : \"" << contact->getNbOffice() << "\"," << std::endl;
	file << "\"nbOther\" : \"" << contact->getNbOther() << "\"," << std::endl;
	file << "\"emailAddress\" : \"" << contact->getEmailAddress() << "\"," << std::endl;
	file << "\"postalAdress\" : \"" << contact->getPostalAdress() << "\"," << std::endl;
	file << "\"birthday\" : \"" << contact->getBirthdayDateString() << "\"," << std::endl;
	file << "\"notes\" : \"" << contact->getNotes() << "\"," << std::endl;
	file << "}" << std::endl;

	// close the file
	file.close();
}

// Add or change an existant file
void DALFileManager::addingModifyNewFile(Contact *contact, int caller)
{
	std::string filePath = contact->getFilePathFromContact();

	boost::filesystem::path p(filePath);
	// if the file doesn't exist => the file is created
	if (boost::filesystem::exists(p) == false)
	{
		std::ofstream file(filePath);

		// if the file doesn't exist => there is an error
		if (boost::filesystem::exists(p) == false)
		{
			std::cerr << "DALFileManager - addingModifyNewFile - fail created new file." << std::endl;
		}

		writeAllTheTypeOfTheContactFile(contact, filePath);
	}
	else
	{
		if (caller == 2)
			writeAllTheTypeOfTheContactFile(contact, filePath);
		else
			std::cout << "This contact already exist !" << std::endl;
	}
}

// extract the content of a file
//** check existing file
//** decrypt content file
//** separate into line the decrypt buffer
void DALFileManager::stockContentsFromFile(std::string pth, std::stringstream* lines)
{
	std::string longPath = Global::getInstance().getFolderContactPath();
	longPath.append("/");
	longPath.append(pth);
	boost::filesystem::path p(longPath);

	//** check if the file exist
	if (boost::filesystem::exists(p) == true)
	{
		std::ifstream ifile(longPath); //, std::ios::in | std::ios::binary);
		ifile.open(longPath);
		if (!ifile.is_open())
		{
			std::cerr << "DALFileManager - displayContactInformation - error file not open." << std::endl;
		}

		if (ifile.eof())
		{
			std::cerr << "DALFileManager - displayContactInformation - fail to read the contact file." << std::endl;
		}

		//** call DALScrypte to decrypt buff before using it
		// read crypte buff
		std::streambuf* bufLines = ifile.rdbuf();
		// decrypt the buffer and rewrite on it the decrypt content

		//** cut into lines the decrypt buffer
		std::streamsize sizeFile = bufLines->pubseekoff(0, ifile.end);
		bufLines->pubseekoff(0, ifile.beg);

		char* buff = new char[sizeFile];

		sizeFile = bufLines->sgetn(buff, sizeFile);
		lines->str(buff);

		ifile.close();
		delete[] buff;
	}
}

// erase a file from a contact object
void DALFileManager::eraseContactFile(Contact* cont)
{
	std::string filePath (cont->getFileNameFromContact());
	std::string longPath = Global::getInstance().getFolderContactPath();
	longPath.append("/");
	longPath.append(filePath);
	boost::filesystem::path p(longPath);

	if (boost::filesystem::exists(p) == true)
	{
		boost::filesystem::remove(p);
	}
}

// check if the file exist
// entry : fileName of the path
// return : true if the path exist and false otherwise
bool DALFileManager::checkExistingFile(std::string fileName)
{
	std::string longPath = Global::getInstance().getFolderContactPath();
	longPath.append("/");
	longPath.append(fileName);
	boost::filesystem::path p(longPath);

	return boost::filesystem::exists(p);
}