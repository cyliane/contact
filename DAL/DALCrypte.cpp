// crypte.cpp
// use the Rijndael algorithme to crypte all the contact files


#include "DALCrypte.hpp"

// #################### INSTANCE #################### //

DALCrypte* DALCrypte::m_instance = nullptr;

DALCrypte& DALCrypte::getInstance()
{
	if (nullptr == m_instance)
	{
		m_instance = new DALCrypte();
	}

	return *m_instance;
}

void DALCrypte::kill()
{
	if (nullptr != m_instance)
	{
		delete m_instance;
		m_instance = nullptr;
	}
}