//crypte.hpp
//
#ifndef DALCRYPTE_HPP
#define DALCRYPTE_HPP



class DALCrypte
{
public:
	~DALCrypte() { }
	static DALCrypte& getInstance();
	static void kill();


private:
	DALCrypte() { }

	static DALCrypte* m_instance;

};



#endif //DALCRYPTE_HPP