// contact.cpp

#include <sstream>
#include <list>

#include "contact.hpp"
#include "global.hpp"

Contact::Contact() : Contact(0)
{}

Contact::Contact(int id)
{
	setBirthdayDate("0000", "00", "00");
}

Contact::Contact(std::string lastname, std::string firstname)
{
	setBirthdayDate("0000", "00", "00");
	_lastname = lastname;
	_firstname = firstname;
}

Contact::~Contact()
{

}

template <typename Container>
void
Contact::stringtok(Container &container, std::string const &in, const char * const delimiters)
{
    const std::string::size_type len = in.length();
	  std::string::size_type i = 0;

    while (i < len)
    {
		// Eat leading whitespace
		i = in.find_first_not_of(delimiters, i);
		if (i == std::string::npos)
		  return;   // Nothing left but white space

		// Find the end of the token
		std::string::size_type j = in.find_first_of(delimiters, i);

		// Push token
		if (j == std::string::npos)
		{
		  container.push_back(in.substr(i));
		  return;
		}
		else
		  container.push_back(in.substr(i, j-i));

		// Set up for next loop
		i = j + 1;
    }
}

void Contact::setBirthdayDateFromString(std::string str)
{
	std::string day, month, year;
	std::list<std::string> ls;

	stringtok(ls, str, "-");

	if (!ls.empty())
	{
		day.append(ls.front());
		ls.pop_front();
		month.append(ls.front());
		ls.pop_front();
		year.append(ls.front());
		ls.pop_front();
	}
	else
	{
		day.clear();
		month.clear();
		year.clear();
	}

	Contact::setBirthdayDate(year, month, day);
}

void Contact::setBirthdayDate(std::string year, std::string month, std::string day)
{
	struct tm tmStruct;
	std::string res;

	if (!(year.empty()) || (!month.empty()) || (!day.empty()))
	{
		res.append(year);
		res.append("-");
		res.append(month);
		res.append("-");
		res.append(day);

		strptime(res.c_str(), "%Y-%m-%d", &tmStruct);

		_birthdayDate = tmStruct;
		_birthdayDateString.assign(day);
		_birthdayDateString.append("-");
		_birthdayDateString.append(month);
		_birthdayDateString.append("-");
		_birthdayDateString.append(year);
	}
	else
	{
		_birthdayDateString.clear();
	}
}

// return the name of the element contact from the number of the line in the contact file
std::string Contact::witchElementContactIs(int numberElement)
{
	std::string witchElementIs;

	switch (numberElement) {
		case 1:
			witchElementIs.assign("lastname");
			break;

		case 2:
			witchElementIs.assign("firstname");
			break;

		case 3:
			witchElementIs.assign("nickname");
			break;

		case 4:
			witchElementIs.assign("nbCellPhone");
			break;

		case 5:
			witchElementIs.assign("nbHome");
			break;

		case 6:
			witchElementIs.assign("nbOffice");
			break;

		case 7:
			witchElementIs.assign("nbOther");
			break;

		case 8:
			witchElementIs.assign("emailAddress");
			break;

		case 9:
			witchElementIs.assign("postalAdress");
			break;

		case 10:
			witchElementIs.assign("birthday");
			break;

		case 11:
			witchElementIs.assign("notes");
			break;

		default:
			break;
	}

	return witchElementIs;
}

// show the lastname and the firstname of a contact
void Contact::showNames()
{
	std::cout << _firstname << " " << _lastname;
}

// return the name of a contact : lastName_firstName
std::string Contact::getFileNameFromContact()
{
	std::string fullName;

	fullName.assign(_lastname);
	fullName.append("_");
	fullName.append(_firstname);

	return fullName;
}

std::string Contact::getFilePathFromContact()
{
	std::string filePath = Global::getInstance().getFolderContactPath();
	filePath.append("/");
	filePath.append(std::to_string(_id));

	return filePath;
}