// Transverse/global.hpp

#ifndef GLOBAL_HPP
#define  GLOBAL_HPP

#include <string>

class Global
{

public:
	// function of the singleton
	static Global& getInstance();

	static void kill();
	~Global() {}

	// function to get the name of the main directory
	std::string getMainDirPath();

	// function to get the name of the contact files folder
	std::string getFolderContactPath();

	// getter & setter for the id of the contact vector
	int getNextId() { return _nextId; }
	void setNextId() { _nextId = _nextId + 1; }
	void initNextId(int nextId) { _nextId = nextId; }

private:
	Global():
		g_dirMain_path(),
		g_dirContactFile_path()
		{ _nextId = 1; }

	static Global *_singleton;

	std::string g_dirMain_path;
	std::string g_dirContactFile_path;
	int _nextId;
};



#endif // GLOBAL_HPP