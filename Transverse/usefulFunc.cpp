// Transverse/usefulFunc.cpp

#include "usefulFunc.hpp"

UsefulFunc *UsefulFunc::_singleton = nullptr;

UsefulFunc& UsefulFunc::getInstance()
{
	if (nullptr == _singleton)
	{
		_singleton = new UsefulFunc();
	}

	return *_singleton;
}

void  UsefulFunc::kill()
{
	if (nullptr != _singleton)
	{
		delete _singleton;
		_singleton = nullptr;
	}
}

int UsefulFunc::stringToNumber(const std::string& text)
{
	std::stringstream ss(text);
	int result;

	return ss >> result ? result : -42;
}

int UsefulFunc::goodNumber(std::string text)
{
	std::string const res = text;

	return stringToNumber(res);
}