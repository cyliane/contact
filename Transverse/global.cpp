// global and constantes variables of the project

#include <pwd.h>
#include <sstream>
#include <iostream>
#include <unistd.h>

#include "global.hpp"

Global *Global::_singleton = nullptr;

Global& Global::getInstance()
{
	if (nullptr == _singleton)
	{
		_singleton = new Global();
	}

	return *_singleton;
}

void  Global::kill()
{
	if (nullptr != _singleton)
	{
		delete _singleton;
		_singleton = nullptr;
	}
}

// name of the directory use for the project
std::string Global::getMainDirPath()
{
	if (g_dirMain_path.empty())
	{
		struct passwd *pw = getpwuid(getuid());
		const char *home_dir = pw->pw_dir;
		g_dirMain_path.append(home_dir);
		g_dirMain_path.append("/repo/contact");
	}

	return g_dirMain_path;
}

// name of the directory of the contact files
std::string Global::getFolderContactPath()
{
	if (g_dirContactFile_path.empty())
	{
		struct passwd *pw = getpwuid(getuid());
		const char *home_dir = pw->pw_dir;
		g_dirContactFile_path.append(home_dir);
		g_dirContactFile_path.append("/repo/contact/files");
	}

	return g_dirContactFile_path;
}
