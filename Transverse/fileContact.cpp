// File class

#include <fstream>
#include <iostream>

#include "global.hpp"
#include "fileContact.hpp"

FileContact::FileContact(std::string fileName)
{
	setFileName(fileName);
	setPath();
	// _pointerTofile = nullptr;
	cutToVect();
}

FileContact::~FileContact()
{
}

// setter of the unique pointer : _pointerTofile
// void File::searchPointerFile()
// {
// 	if (_pointerTofile == nullptr)
// 	{
// 		// set _pointerToFile at the begining of the file
// 		_pointerTofile = fopen(_fileName, "r" | std::ifstream::binary);
// 	}
// 	else if (_pointerTofile != _fileStream.eof())
// 	{
// 		// set the new position of the pointer to pos + 16 bytes
// 		if ((SEEK_CUR+16) >_lenght)
// 		{
// 			long size = ftell(_pointerTofile);
// 			// check the number of byte need to be to 16 bytes
// 			int byteBourrage = _lenght - size;
// 			fseek(_pointerTofile, byteBourrage, SEEK_END);
// 		}
// 		else
// 		{
// 			fseek(_pointerTofile, 16, SEEK_CUR);
// 		}
// 	}
// 	else
// 	{
// 		// the is finish => close it
// 		fclose(_pointerTofile);
// 	}
// }


// setter of the current file name to use : _fileName
void FileContact::setFileName(std::string fileName)
{
	std::string longPath = Global::getInstance().getFolderContactPath();
	longPath.append("/");
	longPath.append(fileName);

	_fileName.assign(longPath);
}

// setter of the current lenght of the file
void FileContact::checkFileLenght()
{
	std::ifstream  fstream(_fileName, std::ifstream::binary);

	if (fstream)
	{
		fstream.seekg(0, fstream.end);
		_lenght = fstream.tellg();
	}
}

// manage to cut to vector the file
void FileContact::cutToVect()
{
	UsefulFunc usefulFunc = UsefulFunc::getInstance();

	UsefulFunc::vectEightBytes* vectBytes = new UsefulFunc::vectEightBytes();

	char ch = '\0';
	int  nbBit = 0, nbBourrBit;
	std::fstream fin(_filePath, std::fstream::in);

	for (; fin >> std::noskipws >> ch; nbBit++)
	{
		if (nbBit < 16)
		{
			usefulFunc.eightBytes.item[nbBit] = ch;
		}
		else
		{
			// add to the vector
			vectBytes->push_back(usefulFunc.eightBytes);

			// restart for the next package
			usefulFunc.eightBytes.item[0] = ch;
			nbBit = 0;
		}
	}

	// check how many character there is in the current packages
	nbBit--;
	if (nbBit < 15)
	{
		nbBourrBit = 15 - nbBit;
		ch = '\0';
		nbBit++;
		for(; nbBourrBit != 0; nbBourrBit--, nbBit++)
		{
			usefulFunc.eightBytes.item[nbBit] = ch;
		}
	}

	vectBytes->push_back(usefulFunc.eightBytes);

	// put in the vect the content of vectBytes
	_vectEightBytes = (*vectBytes);
}
