// file class

#include <string>
#ifndef FILECONTACT_HPP
#define FILECONTACT_HPP

#include <vector>
#include <memory>
#include <fstream>
#include <stdio.h>
#include <boost/filesystem.hpp>

#include "../Transverse/usefulFunc.hpp"


class FileContact
{
	public:
		FileContact(std::string fileName);
		~FileContact();

		// getter & setter functions
		std::string getFilePath() { return _filePath; }
		void setFilePath(std::string filePath) { _filePath = filePath; }

		// getter/setter _filename
		std::string getFileName() { return _fileName; }
		void setFileName(std::string fileName);

		// getter/setter _path
		boost::filesystem::path getPath() { return _path; }
		void setPath() { boost::filesystem::path _path(_fileName); }

		//getter _vectHeightBytes
		UsefulFunc::vectEightBytes getVectEightBytes() { return _vectEightBytes; }

	private:
		std::string _filePath;

		// id of the next char to use
		// std::unique_ptr<FILE> _pointerToFile;

		// current file using to crypt or decrypt
		std::string _fileName;

		// current file path use to crypt and decrypt
		boost::filesystem::path _path;

		// current lenght of the file
		int _lenght;

		// current part of the file to use
		UsefulFunc::vectEightBytes _vectEightBytes;

		// cut the file to 16 octects;
		void cutToVect();

		// get the lenght of the file
		void checkFileLenght();

		// use a pointer to know where you are in the file
		// void searchPointerFile();
};

#endif /* FILECONTACT_HPP */