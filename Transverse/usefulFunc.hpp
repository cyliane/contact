// Transverse/usefulFunc.hpp

#ifndef USEFULFUNC
#define USEFULFUNC

#include <string>
#include <sstream>
#include <vector>


class UsefulFunc
{
	public:
		// struct that contains non copyable or assignable items of the vector
		struct EightBytes {
			char item[128];
		};

		typedef std::vector<UsefulFunc::EightBytes> vectEightBytes;

		// function of the singleton
		static UsefulFunc& getInstance();

		static void kill();
		~UsefulFunc() {}

		int goodNumber(std::string text);

		EightBytes eightBytes;


	private:
		UsefulFunc() { }

		static UsefulFunc *_singleton;
		// convert a text to a number
		int stringToNumber(const std::string& text);

};


#endif //USEFULFUNC