// ABCContact : abstract contact Class

#include <string>

using namespace std;

Class ABCContact
{
public:
	ABCContact(){}
	virtual ~ABCContact(){}

	virtual string getLastname() = 0;
	virtual string getFirstname() = 0;

protected:

};