// contact.hpp
//

#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <iostream>
#include <string>
#include <time.h>
#include <ctime>

class Contact
{
public:
	// contructor/destructor
	Contact();
	Contact(int id);
	Contact(std::string lastname, std::string firstname);
	virtual ~Contact();

	// interface functions
	void showNames();

	//getter/setter
	std::string getLastname() { return _lastname; }
	void setLastname(std::string lastname) { _lastname = lastname; }
	std::string getFirstname() { return _firstname; }
	void setFirstname(std::string firstname) { _firstname = firstname; }
	std::string getNickname() { return _nickname; }
	void setNickname(std::string nickmane){ _nickname = nickmane; }
	std::string getNbCellPhone() { return _nbCellPhone; }
	void setNbCellPhone(std::string nbCellPhone){ _nbCellPhone = nbCellPhone; }
	std::string getNbHome() { return _nbHome; }
	void setNbHome(std::string nbHome){ _nbHome = nbHome; }
	std::string getNbOffice() { return _nbOffice; }
	void setNbOffice(std::string nbOffice){ _nbOffice = nbOffice; }
	std::string getNbOther() { return _nbOther; }
	void setNbOther(std::string nbOther){ _nbOther = nbOther; }
	std::string getEmailAddress() { return _emailAdress; }
	void setEmailAdress(std::string emailAddress){ _emailAdress = emailAddress; }
	std::string getPostalAdress() { return _postalAdress; }
	void setPostalAdress(std::string postalAdress){ _postalAdress = postalAdress; }
	std::string getNotes() { return _notes; }
	void setNotes(std::string notes) { _notes = notes; }
	struct tm getBirthdayDate() { return _birthdayDate; }
	std::string getBirthdayDateString() { return _birthdayDateString; }
	void setBirthdayDate(std::string year, std::string month, std::string day);
	void setBirthdayDateFromString(std::string);
	int getId() { return _id; }
	void setId(int id) { _id = id; }

	static std::string witchElementContactIs(int numberElement);
	// return the name of a contact : lastName_firstName
	std::string getFileNameFromContact();
	// return the path of the file of the contact
	std::string getFilePathFromContact();

private:
	std::string _lastname = "";
	std::string _firstname = "";
	std::string _nickname = "";
	std::string _nbCellPhone = "";
	std::string _nbHome = "";
	std::string _nbOffice = "";
	std::string _nbOther = "";
	std::string _emailAdress = "";
	std::string _postalAdress = "";
	struct tm _birthdayDate;
	std::string _birthdayDateString = "";
	std::string _notes = "";
	int _id;

	template <typename Container>
	void
	stringtok(Container &container, std::string const &in, const char * const delimiters);

};



#endif // CONTACT_HPP