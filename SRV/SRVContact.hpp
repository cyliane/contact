// SRVContact manage the data like contact classes

#ifndef SRVCONTACT_HPP
#define SRVCONTACT_HPP

#include <time.h>
#include <ctime>

class Contact;
class DALFileManager;

class SRVContact
{

public:
	~SRVContact() {}

	static SRVContact& getInstance();

	static void kill();

	//** getter
	std::vector<Contact*> getContactList() { return _contactList; }

	// add new contact to the vector element list
	void addNewContactToVect(Contact* newContact);

	void askingContactInformation();
	void askingWitchElementToChange(Contact* cont);
	// remove an element from a contact
	void removeElementFromContact(Contact* cont);
	// remove a contact
	void eraseContact(Contact* cont);
	// display a contact
	void displayContactInformation(Contact* cont);
	// display the contact list
	void displayVectContactList();

private:
	SRVContact();

	static SRVContact* m_instance;

	std::vector<Contact*> _contactList;

	// creating the new contact file
	void creatingFileContact(Contact *con, DALFileManager *file);
	// little function to manage the new information of the contact
	std::string askAndRegisterTheInformation(std::string nameInformation, int &stop);
	// asking to the user all the informations need to create a full contact
	void askingComplementaryInformations(Contact *con);
	// check the string and keep the end of the line with data of the contact
	// prototype line : << "dataName" : "dataToKeep", >>
	std::string extractContactDataFromLine(char buff[256]);
	// get the element of a contact from the number draw to the console
	std::string getElementDataFromNumberElement(Contact* cont, int numberElement);
	//Change an element of the contact
	void changeElementContact(Contact* cont, int witchLines);
	// extract the data from the stream of the files
	void extractContactDataFromStream(std::stringstream* lines, Contact* contact);
	// Save an element to a contact object
	void saveElementToObject(Contact* cont, int nb, std::string elt);
	// Add new object contact in the contact vector sorting by alphabetic order
	void addNewObjectToVect(Contact* con);
	// erase a object contact in the contact vector sorting by alphabetic order
	void eraseObjectContactToVect(std::string contName);
	// Change or remove an element of a contact
	void changeOrRemoveElementFromContact(Contact* cont, int changeOrRemove);
	//remove an element of the contact
	void removeElementContact(Contact* cont, int witchLines);
	// check the change of the birthday element contact
	std::string birthdayCaseChangeElement();
	// sort _contactList by alphabetic order
	void sortByAlphabeticOrderContactList();
	static bool sortContactFromName(Contact* first, Contact* second);
};


#endif /* SRVCONTACT_HPP */