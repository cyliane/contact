#include <regex>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

#include "SRVContact.hpp"
#include "../Transverse/global.hpp"
#include "../DAL/DALFileManager.hpp"
#include "../Transverse/contact.hpp"
#include "../Transverse/usefulFunc.hpp"

SRVContact* SRVContact::m_instance = nullptr;

SRVContact& SRVContact::getInstance()
{
	if (nullptr == m_instance)
	{
		m_instance = new SRVContact();
	}

	return *m_instance;
}


void SRVContact::kill()
{
	if (nullptr != m_instance)
	{
		delete m_instance;
		m_instance = nullptr;
	}
}

bool SRVContact::sortContactFromName(Contact* first,Contact* second)
{
	return ((first->getFileNameFromContact()).compare(second->getFileNameFromContact()) < 0);
}

// sort _contactList by alphabetic order
void SRVContact::sortByAlphabeticOrderContactList()
{
	sort(_contactList.begin(), _contactList.end(),
		SRVContact::sortContactFromName);
}

//** make a list of the files present inside the contact folder
//** create a contact for each contact file
//** put the pointer of the contact to the vector
SRVContact::SRVContact()
{
	//** make a list of the files present inside the contact folder
	typedef std::vector<std::string> svec;
	svec contactVect = DALFileManager::getInstance().getVectContactList();

	if (!contactVect.empty())
	{
		//** create a contact for each contact file
		std::stringstream lines (std::stringstream::in | std::stringstream::out);
		Global& gl = Global::getInstance();

		// init Global::_nextId
		Global::getInstance().initNextId(std::stoi(contactVect.back()));

		for (svec::const_iterator it = contactVect.begin(); it != contactVect.end(); ++it)
		{
			DALFileManager::getInstance().stockContentsFromFile((*it), &lines);
			Contact *newContact = new Contact();

			extractContactDataFromStream(&lines, newContact);

			// ** put the pointer of the contact to the vector
			newContact->setId(gl.getNextId());
			gl.setNextId();
			_contactList.push_back(newContact);
		}

		// sort _contactList by alphabetic order
		sortByAlphabeticOrderContactList();
	}
}

// add new contact in the list
void SRVContact::addNewContactToVect(Contact* contact)
{
	//** insert the new contact to the vect
	_contactList.push_back(contact);
}

// creating the new contact file
void SRVContact::creatingFileContact(Contact *con, DALFileManager *file)
{
	file->addingModifyNewFile(con, 1);
}

// little function to manage the new information of the contact
std::string SRVContact::askAndRegisterTheInformation(std::string nameInformation, int &stop)
{
	std::string input;

	std::cout << nameInformation << " : ";
	getline(std::cin, input);

	if (input.empty() || (input.compare("##") == 0))
	{
		if (input.compare("##") == 0)
		{
			stop = 13;
		}

		return "";
	}
	else
	{
		return input;
	}
}

// asking to the user all the informations need to create a full contact
void SRVContact::askingComplementaryInformations(Contact *con)
{
	std::string output, year, month, day;
	int stop = 42;

	getline(std::cin, output);
	output = askAndRegisterTheInformation("nickname", stop);
	if (stop == 13)
		return;
	else
		con->setNickname(output);
	output = askAndRegisterTheInformation("nbCellPhone", stop);
	if (stop == 13)
		return;
	else
		con->setNbCellPhone(output);
	output = askAndRegisterTheInformation("nbHome", stop);
	if (stop == 13)
		return;
	else
		con->setNbHome(output);
	output = askAndRegisterTheInformation("nbOffice", stop);
	if (stop == 13)
		return;
	else
		con->setNbOffice(output);
	output = askAndRegisterTheInformation("nbOther", stop);
	if (stop == 13)
		return;
	else
		con->setNbOther(output);
	output = askAndRegisterTheInformation("emailAddress", stop);
	if (stop == 13)
		return;
	else
		con->setEmailAdress(output);
	output = askAndRegisterTheInformation("postalAdress", stop);
	if (stop == 13)
		return;
	else
		con->setPostalAdress(output);
	output = askAndRegisterTheInformation("notes", stop);
	if (stop == 13)
		return;
	else
		con->setNotes(output);

	std::cout << "birthday : " << std::endl;
	output = askAndRegisterTheInformation("year", stop);
	if (stop == 13)
	{
		con->setBirthdayDate("", "", "");
		return;
	}
	else
	{
		year = output;
	}

	output = askAndRegisterTheInformation("month", stop);
	if (stop == 13)
	{
		con->setBirthdayDate(output, "", "");
		return;
	}
	else
	{
		month = output;
	}

	output = askAndRegisterTheInformation("day", stop);
	if (stop == 13)
	{
		con->setBirthdayDate(year, output, "");
		return;
	}
	else
	{
		day = output;
	}

	con->setBirthdayDate(year, month, day);
}

// Add new object contact in the contact vector sorting by alphabetic order
void SRVContact::addNewObjectToVect(Contact* con)
{
	std::vector<Contact*>::iterator itFirst = _contactList.begin();
	std::string objName, newName;
	int askOk = 0;

	// case if the vector is not empty
	if (_contactList.size() > 0)
	{

		newName = con->getFileNameFromContact();

		// first case for this new element
		objName.assign((*itFirst)->getFileNameFromContact());

		if (newName.compare(objName) < 0)
		{
			_contactList.insert(itFirst, con);
			askOk++;
		}

		// classique case
		if (askOk == 0)
		{
			for(std::vector<Contact*>::iterator it = itFirst+1;
				it != _contactList.end(); ++it)
			{
				objName.assign((*it)->getFileNameFromContact());

				if ((askOk == 0) && (newName.compare(objName) < 0))
				{
					_contactList.insert(it, con);
					askOk++;
				}
			}
		}
	}

	// case add at the end of the vector (last item or empty vector)
	if (askOk == 0)
	{
		_contactList.push_back(con);
	}
}

// create a new contact
void SRVContact::askingContactInformation()
{
	std::string lastname, firstname;
	Contact *con = new Contact(lastname, firstname);

	std::cout << "Please give the informations for the new contact." << std::endl;
	std::cout << "lastname, firstname, nickname, cellPhone number, home phone number, "
				"office phone number, other phone number, email adress, postal adress, "
				"notes, birthday date." << std::endl;
	std::cout << "if you don't want to add an information leave blank the field." << std::endl;
	std::cout << "If you want add no more informations use : '##' to save and quit." << std::endl;

	std::cout << "lastname : ";
	std::cin >> lastname;

	std::cout << "firstname : ";
	std::cin >> firstname;

	// if there is no lastname and no firstname => error
	if (lastname.empty() == true && firstname.empty() == true)
	{
		std::cerr << "error : askingContactInformation - missing firstname and lastname" << std::endl;
	}
	else
	{
		// set id of the idContact
		Global& gl = Global::getInstance();
		con->setId(gl.getNextId());
		gl.setNextId();

		con->setLastname(lastname);
		con->setFirstname(firstname);

		askingComplementaryInformations(con);

		// manage the creation of the file with the data of the contact
		DALFileManager& srvFiles = DALFileManager::getInstance();
		creatingFileContact(con, &srvFiles);

		// add the new contact to the vect
		addNewObjectToVect(con);
	}
}


// check the string and keep the end of the line with data of the contact
// prototype line : << "dataName" : "dataToKeep", >>
std::string SRVContact::extractContactDataFromLine(char buff[256])
{
	std::string data, subString;
	char subData[256];
	char* p;

	sscanf(buff, "%*s %*c %*c %s",subData);

	// test if the last character is a "
	p = std::find(subData, subData+255, '"');
	if (p == subData+255)
	{
		std::string k;
		char k1[20];

		subString.assign(buff);
		sscanf(buff, "%s",k1);
		k.append(k1);
		k.append(" : \"");

		subString = subString.substr(k.size(), subString.size());
		subString = subString.substr(0, subString.size()-2);
	}
	else
	{
		sscanf(subData, "%[^\"]", subData);
		subString.assign(subData);

		if ((subString.compare("\",") == 0) || (subString.compare("--") == 0))
		{
			subString.clear();
		}
	}

	return subString;
}


// save an element to a contact
void SRVContact::saveElementToObject(Contact* cont, int nb, std::string elt)
{
		switch (nb) {
		case 1:
			cont->setLastname(elt);
			break;

		case 2:
			cont->setFirstname(elt);
			break;

		case 3:
			cont->setNickname(elt);
			break;

		case 4:
			cont->setNbCellPhone(elt);
			break;

		case 5:
			cont->setNbHome(elt);
			break;

		case 6:
			cont->setNbOffice(elt);
			break;

		case 7:
			cont->setNbOther(elt);
			break;

		case 8:
			cont->setEmailAdress(elt);
			break;

		case 9:
			cont->setPostalAdress(elt);
			break;

		case 10:
			cont->setBirthdayDateFromString(elt);
			break;

		case 11:
			cont->setNotes(elt);
			break;

		default:
			break;
	}
}

// get the element of a contact from the number draw to the console
std::string SRVContact::getElementDataFromNumberElement(Contact* cont, int numberElement)
{
	std::string Element;

	switch (numberElement) {
		case 1:
			Element.assign(cont->getLastname());
			break;

		case 2:
			Element.assign(cont->getFirstname());
			break;

		case 3:
			Element.assign(cont->getNickname());
			break;

		case 4:
			Element.assign(cont->getNbCellPhone());
			break;

		case 5:
			Element.assign(cont->getNbHome());
			break;

		case 6:
			Element.assign(cont->getNbOffice());
			break;

		case 7:
			Element.assign(cont->getNbOther());
			break;

		case 8:
			Element.assign(cont->getEmailAddress());
			break;

		case 9:
			Element.assign(cont->getPostalAdress());
			break;

		case 10:
			Element.assign(cont->getBirthdayDateString());
			break;

		case 11:
			Element.assign(cont->getNotes());
			break;

		default:
			break;
	}

	return Element;
}

// check the change of the birthday element contact
std::string SRVContact::birthdayCaseChangeElement()
{
	std::string year, month, day, dateString, res;

	std::cout << "year : ";
	std::cin >> year;
	std::cout << "month : ";
	std::cin >> month;
	std::cout << "day : ";
	std::cin >> day;

	dateString.assign(year);
	dateString.append("/");
	dateString.append(month);
	dateString.append("/");
	dateString.append(day);


	boost::gregorian::date d(boost::gregorian::from_string(dateString));
	// TODO : rattraper l'erreur pouvant être générée par boost
	boost::gregorian::date::ymd_type ymd = d.year_month_day();

	res.assign(std::to_string(ymd.day));
	res.append("-");
	res.append(std::to_string(ymd.month));
	res.append("-");
	res.append(std::to_string(ymd.year));

	return res;
}

//Change an element of the contact
void SRVContact::changeElementContact(Contact* cont, int witchLines)
{
	std::string newContent, dataElement;

	std::string nameElement (Contact::witchElementContactIs(witchLines));

	dataElement.assign(getElementDataFromNumberElement(cont, witchLines));

	if (dataElement.empty())
	{
		std::cout << "There is no contains in " << nameElement << " for the moment." << std::endl;
	}
	else
	{
		std::cout << "The " << nameElement << " element contains : " << dataElement << std::endl;
	}
	std::cout << "What is the new information about this contact\'s element ?\n";

	// birthday case
	if (witchLines == 10)
	{
		newContent = birthdayCaseChangeElement();
	}
	else
	{
		getline(std::cin, newContent);
		getline(std::cin, newContent);
	}

	std::cout << "The new " << nameElement << " is " << newContent << std::endl;

	if (((witchLines == 1) && ((cont->getFirstname()).empty() == 0)) || ((witchLines == 2) && ((cont->getLastname()).empty() == 0)))
	{
		// you cant put to empty string firstname and lastname
		std::cerr << "You cant put empty string in lastname and firstname contact" << std::endl;
	}
	else
	{
		// save the new content to the object Contact
		saveElementToObject(cont, witchLines, newContent);
	}
}

//remove an element of the contact
void SRVContact::removeElementContact(Contact* cont, int witchLines)
{
	std::string newContent, dataElement, yesOrNo;

	if (((witchLines == 1) && ((cont->getFirstname()).empty() == 0)) || ((witchLines == 2) && ((cont->getLastname()).empty() == 0)))
	{
		std::cout << "You can't remove the name of a contact !" << std::endl;
	}
	else
	{
		std::string nameElement (Contact::witchElementContactIs(witchLines));

		dataElement.assign(getElementDataFromNumberElement(cont, witchLines));

		if (dataElement.empty())
		{
			std::cout << "There is no contains in " << nameElement << " for the moment." << std::endl;
		}
		else
		{
			std::cout << "The " << nameElement << " element contains : " << dataElement << std::endl;
		}

		std::cout << "Are you sure to remove this contact\'s element ? 'y' or 'n'\n";
		std::cin >> yesOrNo;

		if (yesOrNo.compare("y") == 0)
		{
			newContent.assign("");

			saveElementToObject(cont, witchLines, newContent);
		}
	}
}

// Change or remove an element of a contact
void SRVContact::changeOrRemoveElementFromContact(Contact* cont, int changeOrRemove)
{
	int nbElt = 0;
	std::string witchElementToChoose;

	do {
		// display the contact element
		displayContactInformation(cont);

		// display the different possible choices of element to modify or remove
		if (changeOrRemove == 0)
			std::cout << "Give the number of the element that should be modified :" << std::endl;
		else
			std::cout << "Give the number of the element that should be removed :" << std::endl;

		std::cout << "1  : lastname" << std::endl;
		std::cout << "2  : firstname" << std::endl;
		std::cout << "3  : nickname" << std::endl;
		std::cout << "4  : nbCellPhone" << std::endl;
		std::cout << "5  : nbHome" << std::endl;
		std::cout << "6  : nbOffice" << std::endl;
		std::cout << "7  : nbOther" << std::endl;
		std::cout << "8  : emailAddress" << std::endl;
		std::cout << "9  : postalAdress" << std::endl;
		std::cout << "10 : birthday" << std::endl;
		std::cout << "11 : notes" << std::endl;
		std::cout << "q : Quit" << std::endl;
		std::cin >> witchElementToChoose;

		nbElt = UsefulFunc::getInstance().goodNumber(witchElementToChoose);

		if ((nbElt < 0) || (nbElt > 11))
		{
			std::cout << "Choose a valid option !" << std::endl;
		}
		else
		{
			if (witchElementToChoose.compare("q") != 0)
			{
				if (changeOrRemove == 0)
					changeElementContact(cont, nbElt);
				else
					removeElementContact(cont, nbElt);

				// call sort vector contactList if th remove is about "lastname" or "firstname"
				if ((nbElt == 1) || (nbElt == 2))
				{
					sortByAlphabeticOrderContactList();
				}
			}
		}

	} while (witchElementToChoose != "q");
}

// Change an element of a contact
//** Choix d'un élémént d'un contact
//** modification d'un élément dans l'objet
// lors de la sortie de la fonction
//** suppression du fichier
//** réécriture avec le nouvel objet contact
void SRVContact::askingWitchElementToChange(Contact* cont)
{
	//** Choix d'un élémént d'un contact
	//** modification d'un élément dans l'objet
	changeOrRemoveElementFromContact(cont, 0);

	//** suppression du fichier
	//** réécriture avec le nouvel objet contact
	DALFileManager::getInstance().addingModifyNewFile(cont, 2);
}

// remove an element from a contact
//** Choix d'un élémént d'un contact
//** Demande de confirmation
//** suppression d'un élément dans l'objet
// lors de la sortie de la fonction
//** suppression du fichier
//** réécriture avec le nouvel objet contact
void SRVContact::removeElementFromContact(Contact* cont)
{
	//** Choix d'un élémént d'un contact
	//** Demande de confirmation
	//** suppression d'un élément dans l'objet
	changeOrRemoveElementFromContact(cont, 1);

	//** suppression du fichier
	//** réécriture avec le nouvel objet contact
	DALFileManager::getInstance().addingModifyNewFile(cont, 2);
}

// erase a contact in the contact vector sorting by alphabetic order
void SRVContact::eraseObjectContactToVect(std::string contName)
{
	std::vector<Contact*>::iterator itBeforeLast = _contactList.end()-1;
	std::string last, first, objName;
	int askOk = 0;

	// classique case
	for(std::vector<Contact*>::iterator it = _contactList.begin();
		it != itBeforeLast; ++it)
	{
		last = (*it)->getLastname();
		first = (*it)->getFirstname();

		objName.assign(last);
		objName.append("_");
		objName.append(first);

		if ((askOk == 0) && (contName.compare(objName) == 0))
		{
			_contactList.erase(it);
			askOk++;
		}
	}

	// case erase at the end of the vector
	if (askOk == 0)
	{
		_contactList.pop_back();
	}
}

// remove a contact
//** remove the file
//** remove from the contact list
void SRVContact::eraseContact(Contact* cont)
{
	//** remove the file
	DALFileManager::getInstance().eraseContactFile(cont);

	//** remove from the contact list
	eraseObjectContactToVect(cont->getFileNameFromContact());
}

// extract the data from the stream of the files
void SRVContact::extractContactDataFromStream(std::stringstream* lines, Contact* contact)
{
	char buff[256];

	lines->getline(buff, 256);
	lines->getline(buff, 256);
	contact->setLastname(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setFirstname(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setNickname(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setNbCellPhone(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setNbHome(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setNbOffice(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setNbOther(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setEmailAdress(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setPostalAdress(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setBirthdayDateFromString(extractContactDataFromLine(buff));
	lines->getline(buff, 256);
	contact->setNotes(extractContactDataFromLine(buff));
}

// display a contact
void SRVContact::displayContactInformation(Contact* contact)
{
	std::cout << "lastname     : " << contact->getLastname() << std::endl;
	std::cout << "firstname    : " << contact->getFirstname() << std::endl;
	std::cout << "nickname     : " << contact->getNickname() << std::endl;
	std::cout << "nbCellPhone  : " << contact->getNbCellPhone() << std::endl;
	std::cout << "nbHome       : " << contact->getNbHome() << std::endl;
	std::cout << "nbOffice     : " << contact->getNbOffice() << std::endl;
	std::cout << "nbOther      : " << contact->getNbOther() << std::endl;
	std::cout << "emailAddress : " << contact->getEmailAddress() << std::endl;
	std::cout << "postalAdress : " << contact->getPostalAdress() << std::endl;
	std::cout << "birthday     : " << contact->getBirthdayDateString() << std::endl;
	std::cout << "notes        : " << contact->getNotes() << std::endl;
	std::cout << std::endl;
}

// display the list of the contact
void SRVContact::displayVectContactList()
{
	typedef std::vector<Contact*> svec;
	//Contact* contact = new Contact();
	int i = 0;

	if (!_contactList.empty())
	{
		for(svec::const_iterator it = _contactList.begin(); it != _contactList.end(); ++it, i++)
		{
			std::cout << "- " << i << " : " << _contactList[i]->getLastname() << " " << _contactList[i]->getFirstname() << std::endl;
		}
	}

	// saut de ligne pour aérer la liste
	std::cout << std::endl;
}