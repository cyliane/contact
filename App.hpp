// App.hpp


#ifndef APP_HPP
#define APP_HPP

class Contact;
#include <string>

class App
{
public:
	static App&	getInstance();
	static void kill();

	~App() { }

	void launchApp();

private:
	App() {}

	static App*	_singleton;

	// destoy unique instance of global variables
	void killGlobalVariables();
	// create the folder need to stock the different contact files
	void initFolder();
	// manage the creation of the vector of object "Contact" from the files present in Files folder
	void initExistingContactVector();
	// show the list of the contact and choose witch contact to show
	void ChooseWitchContact(char &toClose);
	// manage the adding of the new contact
	void addNewContact();
	// get the name of the file to check
	Contact* getContactFromVect(uint index);
	// change a element of a contact
	void changeContact(uint witchContact);
	// remove an element from a contact
	void removeElementFromContact(uint witchContact);
	// remove a contact
	void eraseContact(uint witchContact);
	// display a contact
	void displayContactInformation(uint witchContact);
};

#endif /* APP_HPP */