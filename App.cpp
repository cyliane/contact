// app.cpp


#include <pwd.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>

#include "App.hpp"

#include "DAL/DALCrypte.hpp"
#include "SRV/SRVContact.hpp"
#include "Transverse/global.hpp"
#include "DAL/DALFileManager.hpp"
#include "Transverse/contact.hpp"
#include "Transverse/usefulFunc.hpp"

App *App::_singleton = nullptr;

App& App::getInstance()
{
	if (nullptr == _singleton)
	{
		_singleton = new App();
	}

	return *_singleton;
}

void App::kill()
{
	if (nullptr != _singleton)
	{
		delete _singleton;
		_singleton = nullptr;
	}
}


// destoy unique instance of global variables
void App::killGlobalVariables()
{
	Global::kill();
	DALFileManager::kill();
	SRVContact::kill();
	UsefulFunc::kill();
	DALCrypte::kill();
}

// create the folder need to stock the different contact files
void App::initFolder()
{
	//creation du dossier contenant les fichiers des contacts
	std::string mainDirPath = Global::getInstance().getMainDirPath();
	mainDirPath.append("/files");
	boost::filesystem::path p (mainDirPath);

	if (boost::filesystem::exists(p) == false)
	{
		boost::filesystem::path dir(mainDirPath);
		if (boost::filesystem::create_directory(mainDirPath) == false)
		{
			std::cerr << "App -- initFolder -- create folder -- fail" << std::endl;
		}
	}
}

// manage the creation of the vector of object "Contact" from the files present in Files folder
void App::initExistingContactVector()
{
	// call the SRVContact class to create the vector of contact
	SRVContact& cont = SRVContact::getInstance();

	std::vector<Contact*> vect = cont.getContactList();

	if (vect.empty())
	{
		std::cout << "There is no contact in this application." << std::endl;
	}
}

// manage the adding of the new contact
void App::addNewContact()
{
	// call the function to ask the information of the new contact
	SRVContact::getInstance().askingContactInformation();
}


// get the name of the file to check
Contact* App::getContactFromVect(uint index)
{
	SRVContact& cont = SRVContact::getInstance();
	std::vector<Contact*> contVect = cont.getContactList();
	Contact* contact = nullptr;

	// check if the index is in the vector
	if ((index >= 0) && (index < contVect.size()))
	{
		contact = contVect[index];
	}
	else
	{
		std::cout << "This contact doesn't exist !" << std::endl;
		contact = nullptr;
	}

	return contact;
}

// change a element of a contact
void App::changeContact(uint witchContact)
{
	// get the contact to change
	Contact* cont = getContactFromVect(witchContact);

	// test if the contact exist
	if (cont != nullptr)
	{
		SRVContact::getInstance().askingWitchElementToChange(cont);
	}

}

// remove an element from a contact
void App::removeElementFromContact(uint witchContact)
{
	// get the contact to change
	Contact* cont = getContactFromVect(witchContact);

	// test if the contact exist
	if (cont != nullptr)
	{
		SRVContact::getInstance().removeElementFromContact(cont);
	}
}

// remove a contact
void App::eraseContact(uint witchContact)
{
	std::string ask = "n";

	std::cout << "Are you sure ? 'y' or 'n'" << std::endl;
	std::cin >> ask;

	if (ask.compare("y") == 0)
	{
		// get the contact to erase
		Contact* cont = getContactFromVect(witchContact);

		// test if the contact exist
		if (cont != nullptr)
		{
			SRVContact::getInstance().eraseContact(cont);
		}
	}
}

// display a contact
void App::displayContactInformation(uint witchContact)
{
	// get the contact to display
	Contact* cont = getContactFromVect(witchContact);

	// test if the contact exist
	if (cont != nullptr)
	{
		SRVContact::getInstance().displayContactInformation(cont);
	}
}

// show the list of the contact and choose witch contact to show
void App::ChooseWitchContact(char &toClose)
{
	SRVContact& scon = SRVContact::getInstance();
	std::string toChoose = "";

	while (toChoose != "q")
	{
		if (!scon.getContactList().empty())
		{
			// affichage des nom et prénom de chaque contact présent dans la base
			scon.displayVectContactList();
		}

		std::cout << "a : add a contact" << std::endl;
		std::cout << "c + number: change an element of the contact number 'X'" << std::endl;
		std::cout << "r + number: remove an element of the contact number 'X'" << std::endl;
		std::cout << "e + number: erase the contact number 'X'" << std::endl;
		std::cout << "d + number: display the contact number 'X'" << std::endl;
		std::cout << "q : quit" << std::endl;
		std::cin >> toChoose;

		char witchLetter = toChoose.at(0);
		int witchContactInt;
		uint witchContact;
		if (toChoose.length() > 1)
			witchContactInt = UsefulFunc::getInstance().goodNumber(toChoose.substr(1));

		if (witchContactInt < 0)
		{
			std::cout << "Choose a valid option please !" << std::endl;
		}
		else
		{
			witchContact = (uint)witchContactInt;
			switch (witchLetter) {
			case 'q':
				toClose = 'q';
				break;
			case 'a':
				addNewContact();
				break;
			case 'c':
				if (toChoose.length() > 1)
					changeContact(witchContact);
				else
					std::cout << "Choose a contact number." << std::endl;
				break;
			case 'r':
				if (toChoose.length() > 1)
					removeElementFromContact(witchContact);
				else
					std::cout << "Choose a contact number." << std::endl;
				break;
			case 'e':
				if (toChoose.length() > 1)
					eraseContact(witchContact);
				else
					std::cout << "Choose a contact number." << std::endl;
				break;
			case 'd':
				if (toChoose.length() > 1)
					displayContactInformation(witchContact);
				else
					std::cout << "Choose a contact number." << std::endl;
				break;
			default:
					std::cout << "Choose a valid option please !" << std::endl;
				break;
			}
		}
	}
}

void App::launchApp()
{
	char toClose = 'd';

	// create the folder need to stock the different contact files
	initFolder();

	// init the vector witch contains the contact list
	initExistingContactVector();

	while (toClose != 'q')
	{

		if (toClose == 'd')
		{
			ChooseWitchContact(toClose);
		}

		std::cout << "d: display the contact list" << std::endl;
		std::cout << "q : quit" << std::endl;
		std::cout << "Whitch action do you want to choose ?" << std::endl;
		std::cin >> toClose;
	}

	// kill the gloables variables
	killGlobalVariables();
}