// contact.cpp

#include "App.hpp"

int main()
{
	App& app = App::getInstance();

	app.launchApp();

	App::kill();

	return 0;
}
